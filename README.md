# salesforce-integration-react-native Repository

> The `salesforce-integration-react-native` repository is very basic [React Native](https://reactnative.dev/) client application
> designed to make RESTful calls to the [`salesforce-integration-service`](https://gitlab.com/johnjvester/salesforce-integration-service)
> repository, which interacts with a [Salesforce](http://www.salesforce.com) instance in order to retrieve (GET)
> `Contact` objects via mobile clients.

## Publications

This repository is related to an article published on DZone.com:

* [Exploring the Salesforce Mobile SDK Using React Native](https://dzone.com/articles/exploring-the-salesforce-mobile-sdk-using-react-na)

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939

## Important Information

This is a very quick React Native application, which expects the `salesforce-integration-service` to be running at 
the following URL:

```shell
http://localhost:9999/contacts
```

For now, if this value needs to change, simply update the following line of code in the `Contacts.js` file to 
reflect the proper URL for the Spring Boot API:

```javascript
axios.get("http://192.168.1.212:9999/contacts")
```

In order to run this application using an Android or iOS (Mac users only) emulator, the proper software must be installed 
and configured on your local machine.  It is possible to use the Expo software and utilize a real Android or iOS device. 
Just remember, the device(s) must be able to connect to the React Native application.  

For more information on how to accomplish these tasks, please review the following URL:

[React Native - Setting up the development environment](https://reactnative.dev/docs/environment-setup)

## Project Dependencies

The following dependencies are part of this project:

* [`react-native-table-component`](https://www.npmjs.com/package/react-native-table-component) - enables effortless use of tables for React Native applications.
* [`axios`](https://www.npmjs.com/package/axios) - promise based HTTP client for the browser and node.js.

## Using This Repository

To use this repository, simply follow the instructions in the [`salesforce-integration-service`](https://gitlab.com/johnjvester/salesforce-integration-service) repository, 
then issue the following command:

```bash
npm install
npm run ios
```

You can also use `npm run android` as well.

Once started the Metro Bundler screen will display in a browser tab:

![Metro Bundler Screen](./MetroBundlerScreen.png)

In the example below, both Android and iOS (iPhone 8) emulators were running.  Once the Expo software was auto-loaded, the
following screen appeared while contacts were being retrieved automatically:

![Mobile Loading Data](./MobileLoadingData.png)

After a hard-coded delay of two seconds (to make sure the loading spinner always displays), the screens are updated as shown 
below:

![Mobile Showing Data](./MobileShowingData.png)

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.
