import React, { Component } from 'react';
import { ActivityIndicator, StyleSheet, Text, View } from "react-native";
import { Table, Row, Rows } from 'react-native-table-component';
import axios from 'axios';

const styles = StyleSheet.create({
    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    head: { height: 40, backgroundColor: '#f1f8ff' },
    headerText: { margin: 6, fontWeight: 'bold' },
    text: { margin: 6 },
    loadingText: { color: '#1798c1', marginTop: 15 }
});

export default class Contacts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            tableHead: ['Name', 'Department', 'Title'],
            tableData: []
        }
    }

    componentDidMount() {
        this.setState({
            loading: true,
            tableData: []
        });

        axios.get("http://192.168.1.212:9999/contacts")
            .then(response => {
                setTimeout(() => {
                    this.setState({
                        tableData: this.createTableData(response.data),
                        loading: false
                    })
                }, 2000)
            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        const state = this.state;
        return (
                <View style={styles.container}>
                    <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
                        <Row data={state.tableHead} style={styles.head} textStyle={styles.headerText}/>
                        <Rows data={state.tableData} textStyle={styles.text}/>
                    </Table>

                    { state.loading &&
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: 30 }}>
                            <ActivityIndicator size='large' color='#1798c1' />
                            <Text style={ styles.loadingText }>Loading Contacts from Salesforce ...</Text>
                        </View>
                    }
                </View>
        )
    }

    createTableData = (contacts) => {
        let tableData = [];
        if (contacts) {
            for (let i = 0; i < contacts.length; i++) {
                tableData.push([contacts[i].Name,
                    contacts[i].Department ? contacts[i].Department : "(not set)",
                    contacts[i].Title ? contacts[i].Title : "(not set)"]);
            }
        }

        return tableData;
    }
}
